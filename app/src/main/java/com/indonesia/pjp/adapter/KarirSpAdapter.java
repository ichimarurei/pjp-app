package com.indonesia.pjp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indonesia.pjp.R;
import com.indonesia.pjp.helper.ClickListener;
import com.indonesia.pjp.helper.Utility;
import com.indonesia.pjp.response.KarirResponse;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by DONI on 8/11/2018.
 */

public class KarirSpAdapter extends RecyclerView.Adapter<KarirSpAdapter.ViewHolder> {

    private List<KarirResponse.SuratPeringartan> dataList;
    private Context context;
    final ClickListener clickListener;

    public KarirSpAdapter(List<KarirResponse.SuratPeringartan> dataList, Context context, ClickListener clickListener) {
        this.dataList = dataList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_karir_sp, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nomorTextView.setText(dataList.get(position).getNomor());
        holder.keTextView.setText(dataList.get(position).getKe());
        holder.waktuTextView.setText(Utility.stripHtml(dataList.get(position).getTanggal()));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        WeakReference<ClickListener> clickListenerRef;

        @BindView(R.id.nomorTextView)
        TextView nomorTextView;

        @BindView(R.id.keTextView)
        TextView keTextView;

        @BindView(R.id.waktuTextView)
        TextView waktuTextView;

        ViewHolder(final View itemView) {
            super(itemView);
            clickListenerRef = new WeakReference<>(clickListener);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListenerRef.get().onItemClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListenerRef.get().onItemLongClick(v, getAdapterPosition());
            return true;
        }
    }
}