package com.indonesia.pjp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indonesia.pjp.R;
import com.indonesia.pjp.activity.KaryawanActivity;
import com.indonesia.pjp.activity.ProyekActivity;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by doni.wahyu on 6/27/2019.
 */

public class ProyekKaryawanFilterBottomSheetDialog extends BottomSheetDialogFragment {
    private BottomSheetFilterListener mListener;
    String proyekId = "";
    String proyekText = "";

    String karyawanId = "";
    String karyawanText = "";

    @OnClick(R.id.applyButton)
    public void applyButtonClick(Button button) {
        mListener.onButtonFilterClicked(proyekId, proyekText, karyawanId, karyawanText);
        dismiss();
    }

    @OnClick(R.id.resetButton)
    public void resetButtonClick(Button button) {
        mListener.onButtonrResetClicked();
        dismiss();
    }

    public void setListener(BottomSheetFilterListener listener) {
        this.mListener = listener;
    }

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        chooseProyek();
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        if (Utility.isEmpty(proyekTextView)) {
            chooseProyek();
        } else {
            chooseKaryawan();
        }
    }

    void chooseKaryawan() {
        Intent intent = new Intent(getContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT)
                .putExtra("proyekId", proyekId);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    void chooseProyek() {
        Intent intent = new Intent(getContext(), ProyekActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                proyekTextView.setText(data.getStringExtra("text"));
                proyekId = data.getStringExtra("id");
                proyekText = data.getStringExtra("text");
            }else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                karyawanTextView.setText(data.getStringExtra("text"));
                karyawanId = data.getStringExtra("id");
                karyawanText = data.getStringExtra("text");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_project_karyawan_filter, container, false);
        ButterKnife.bind(this, v);

        proyekId = getArguments().getString("proyekId");
        proyekText = getArguments().getString("proyekText");
        proyekTextView.setText(proyekText);

        return v;
    }

    public interface BottomSheetFilterListener {
        void onButtonFilterClicked(String proyekId, String proyekText, String karyawanId, String karyawanText);

        void onButtonrResetClicked();
    }
}

