package com.indonesia.pjp.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.animation.Animation;

import com.indonesia.pjp.helper.ApiInterface;
import com.indonesia.pjp.helper.ConnectionDetector;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.helper.Utility;


/**
 * Created by doni.wahyu on 11/27/2018.
 */

public class BaseFragment extends Fragment {
    public ConnectionDetector cd;
    public ApiInterface api;
    public ProgressDialog pDialog;
    public Animation animShow;
    public Animation animHide;
    public SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getContext());
        api = Utility.getInterfaceService();
        sessionManager = new SessionManager(getContext());
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void showProgressDialog(String message) {
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage(message);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage(Constants.DEFAULT_LOADING_MESSAGE);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void dismissProgressDialog() {
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }

    public void toast(String message) {
        Utility.makeToast(getContext(), message);
    }

    void startActivityIntent(Class c) {
        startActivity(new Intent(getContext(), c));
    }
}
