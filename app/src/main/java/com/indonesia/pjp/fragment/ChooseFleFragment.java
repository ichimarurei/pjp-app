package com.indonesia.pjp.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indonesia.pjp.R;
import com.indonesia.pjp.helper.Utility;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by doni.wahyu on 11/16/2018.
 */

public class ChooseFleFragment extends BottomSheetDialogFragment {
    private BottomSheetFileChooserListener mListener;

    @OnClick(R.id.cameraTextView)
    public void cameraTextView(TextView textView) {
        EasyImage.openCamera(this, 0);
    }

    @OnClick(R.id.galleryTextView)
    public void galleryTextView(TextView textView) {
        EasyImage.openGallery(this, 0);
    }

    @OnClick(R.id.cancelTextView)
    public void cancelTextView(TextView textView) {
        dismiss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_choose_file, container, false);
        ButterKnife.bind(this, v);

        Utility.checkPermissions(getActivity());
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                e.printStackTrace();
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                if (imageFile != null) {
                    mListener.onFileSelected(imageFile);
                    dismiss();
//                    ((InputPresensiActivity) getActivity()).addFileUpload(imageFile);
                } else {
                    Utility.makeToast(getContext(), "Error Load Image");
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
            }
        });
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    public void setListener(BottomSheetFileChooserListener listener) {
        this.mListener = listener;
    }

    public interface BottomSheetFileChooserListener {
        void onFileSelected(File imageFile);
    }
}


