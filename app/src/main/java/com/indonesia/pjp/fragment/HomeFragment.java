package com.indonesia.pjp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indonesia.pjp.R;
import com.indonesia.pjp.activity.CutiActivity;
import com.indonesia.pjp.activity.InputCutiActivity;
import com.indonesia.pjp.activity.InputKasKecilActivity;
import com.indonesia.pjp.activity.InputKasbonActivity;
import com.indonesia.pjp.activity.InputKlaimActivity;
import com.indonesia.pjp.activity.InputKpiActivity;
import com.indonesia.pjp.activity.InputLemburActivity;
import com.indonesia.pjp.activity.InputPresensiActivity;
import com.indonesia.pjp.activity.InputPresensiKorlapActivity;
import com.indonesia.pjp.activity.InputPulangActivity;
import com.indonesia.pjp.activity.InputShipmentActivity;
import com.indonesia.pjp.activity.KarirActivity;
import com.indonesia.pjp.activity.KaryawanActivity;
import com.indonesia.pjp.activity.KasKecilActivity;
import com.indonesia.pjp.activity.KasbonActivity;
import com.indonesia.pjp.activity.KlaimActivity;
import com.indonesia.pjp.activity.KpiActivity;
import com.indonesia.pjp.activity.LemburActivity;
import com.indonesia.pjp.activity.LiveLocationActivity;
import com.indonesia.pjp.activity.MyQrCodeActivity;
import com.indonesia.pjp.activity.PresensiActivity;
import com.indonesia.pjp.activity.ProyekActivity;
import com.indonesia.pjp.activity.PulangActivity;
import com.indonesia.pjp.activity.RekapActivity;
import com.indonesia.pjp.activity.ShipmentActivity;
import com.indonesia.pjp.activity.SuratPeringatanActivity;
import com.indonesia.pjp.adapter.MenuAdapter;
import com.indonesia.pjp.helper.ClickListener;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.model.Menu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by doni.wahyu on 9/25/2018.
 */

public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.menuRecyclerView)
    RecyclerView menuRecyclerView;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, null);
        ButterKnife.bind(this, rootView);

        GridLayoutManager layoutManagerMenu =
                new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false);
        menuRecyclerView.setLayoutManager(layoutManagerMenu);

        final List<Menu> menuList = new ArrayList<>();

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_ADMIN)) {
            menuList.add(new Menu(Constants.MENU_PROYEK, R.drawable.truck, true));
            menuList.add(new Menu(Constants.MENU_LIVE_LOCATION, R.drawable.location, true));
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_ADMIN) ||
                sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {

            menuList.add(new Menu(Constants.MENU_KPI, R.drawable.overtime, true));
            menuList.add(new Menu(Constants.MENU_INPUT_KPI, R.drawable.overtime, true));

            menuList.add(new Menu(Constants.MENU_KARYAWAN, R.drawable.employee, true));

            menuList.add(new Menu(Constants.MENU_PRESENSI, R.drawable.click_hand, true));
            menuList.add(new Menu(Constants.MENU_INPUT_PRESENSI, R.drawable.click_hand_add, true));

            menuList.add(new Menu(Constants.MENU_LEMBUR, R.drawable.overtime, true));
            menuList.add(new Menu(Constants.MENU_INPUT_LEMBUR, R.drawable.overtime_add, true));

            menuList.add(new Menu(Constants.MENU_PULANG, R.drawable.click_hand, true));
            menuList.add(new Menu(Constants.MENU_INPUT_PULANG, R.drawable.click_hand_add, true));

            menuList.add(new Menu(Constants.MENU_CUTI, R.drawable.employee_leave, true));
            menuList.add(new Menu(Constants.MENU_INPUT_CUTI, R.drawable.employee_leave_add, true));

            menuList.add(new Menu(Constants.MENU_KLAIM, R.drawable.reimbursement, true));
            menuList.add(new Menu(Constants.MENU_INPUT_KLAIM, R.drawable.reimbursement_add, true));

            menuList.add(new Menu(Constants.MENU_KASBON, R.drawable.kasbon, true));
            menuList.add(new Menu(Constants.MENU_INPUT_KASBON, R.drawable.kasbon_add, true));

            menuList.add(new Menu(Constants.MENU_SHIPMENT, R.drawable.truck, true));
            menuList.add(new Menu(Constants.MENU_INPUT_SHIPMENT, R.drawable.truck, true));

            menuList.add(new Menu(Constants.MENU_KACIL, R.drawable.reimbursement, true));
            menuList.add(new Menu(Constants.MENU_INPUT_KACIL, R.drawable.reimbursement_add, true));

            menuList.add(new Menu(Constants.MENU_SURAT_PERINGATAN, R.drawable.surat_peringatan, true));
            menuList.add(new Menu(Constants.MENU_KARIR, R.drawable.employee, true));

            menuList.add(new Menu(Constants.MENU_REKAP_ABSENSI, R.drawable.click_hand, true));
            menuList.add(new Menu(Constants.MENU_REKAP_LEMBUR, R.drawable.overtime, true));
        }


        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)) {
            menuList.add(new Menu(Constants.MENU_KARYAWAN, R.drawable.employee, true));
            menuList.add(new Menu(Constants.MENU_PRESENSI, R.drawable.click_hand, true));
            menuList.add(new Menu(Constants.MENU_LEMBUR, R.drawable.overtime, true));
            menuList.add(new Menu(Constants.MENU_PULANG, R.drawable.click_hand, true));
            menuList.add(new Menu(Constants.MENU_CUTI, R.drawable.employee_leave, true));
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_PEGAWAI)) {
            menuList.add(new Menu(Constants.MENU_QR_CODE, R.drawable.qrcode, true));
            menuList.add(new Menu(Constants.MENU_PRESENSI, R.drawable.click_hand_add, true));
            menuList.add(new Menu(Constants.MENU_LEMBUR, R.drawable.overtime, true));
            menuList.add(new Menu(Constants.MENU_PULANG, R.drawable.click_hand, true));
            menuList.add(new Menu(Constants.MENU_CUTI, R.drawable.employee_leave, true));
        }

        menuRecyclerView.setAdapter(new MenuAdapter(menuList, getContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_PROYEK)) {
                    startActivityIntent(ProyekActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_LIVE_LOCATION)) {
                    startActivityIntent(LiveLocationActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_KARYAWAN)) {
                    startActivityIntent(KaryawanActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_KPI)) {
                    startActivityIntent(KpiActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_KPI)) {
                    startActivityIntent(InputKpiActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_CUTI)) {
                    startActivityIntent(KaryawanActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_QR_CODE)) {
                    startActivityIntent(MyQrCodeActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_PRESENSI)) {
                    startActivityIntent(PresensiActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_PRESENSI)) {
                    startActivityIntent(InputPresensiActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_PRESENSI_KORLAP)) {
                    startActivityIntent(InputPresensiKorlapActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_LEMBUR)) {
                    startActivityIntent(LemburActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_LEMBUR)) {
                    startActivityIntent(InputLemburActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_PULANG)) {
                    startActivityIntent(PulangActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_PULANG)) {
                    startActivityIntent(InputPulangActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_CUTI)) {
                    startActivityIntent(CutiActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_CUTI)) {
                    startActivityIntent(InputCutiActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_KLAIM)) {
                    startActivityIntent(KlaimActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_KASBON)) {
                    startActivityIntent(KasbonActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_KLAIM)) {
                    startActivityIntent(InputKlaimActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_KASBON)) {
                    startActivityIntent(InputKasbonActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_SHIPMENT)) {
                    startActivityIntent(InputShipmentActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_SHIPMENT)) {
                    startActivityIntent(ShipmentActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_KACIL)) {
                    startActivityIntent(KasKecilActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_INPUT_KACIL)) {
                    startActivityIntent(InputKasKecilActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_SURAT_PERINGATAN)) {
                    startActivityIntent(SuratPeringatanActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_KARIR)) {
                    startActivityIntent(KarirActivity.class);
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_REKAP_ABSENSI)) {
                    startActivity(new Intent(getContext(), RekapActivity.class).putExtra("rekapType", Constants.MENU_REKAP_ABSENSI));
                } else if (menuList.get(position).getMenuName().equalsIgnoreCase(Constants.MENU_REKAP_LEMBUR)) {
                    startActivity(new Intent(getContext(), RekapActivity.class).putExtra("rekapType", Constants.MENU_REKAP_LEMBUR));
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
        
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        getNotificationData();
    }

    @Override
    public void onRefresh() {
        getNotificationData();
    }

    void getNotificationData() {
        /*swipeRefreshLayout.setRefreshing(true);
        recyclerView.setAdapter(null);
        Call<NotificationResponse> service = api.getNotification(sessionManager.retrieve(SessionManager.KEYCODE));

        service.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                NotificationResponse notificationResponse = response.body();

                if (notificationResponse != null) {
                    if (notificationResponse.isSuccess()) {
                        recyclerView.setAdapter(new NotificationAdapter(notificationResponse.getNotificationList(), getContext(), new ClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                            }

                            @Override
                            public void onItemLongClick(View view, int position) {

                            }
                        }));
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                call.cancel();
            }
        });*/
    }
}


