package com.indonesia.pjp.model;

/**
 * Created by doni.wahyu on 8/1/2019.
 */

public class Termin {
    private String id;
    private String text;


    public Termin(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
