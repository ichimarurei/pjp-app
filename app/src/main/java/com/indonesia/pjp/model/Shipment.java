package com.indonesia.pjp.model;

/**
 * Created by doni.wahyu on 6/22/2019.
 */

public class Shipment {
    private String kode;
    private String proyek;
    private String id;
    private String biodata;
    private String waktu;
    private String inap;
    private String skr;
    private String ritase;
    private String insentif;

    public Shipment() {
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getProyek() {
        return proyek;
    }

    public void setProyek(String proyek) {
        this.proyek = proyek;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBiodata() {
        return biodata;
    }

    public void setBiodata(String biodata) {
        this.biodata = biodata;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getInap() {
        return inap;
    }

    public void setInap(String inap) {
        this.inap = inap;
    }

    public String getSkr() {
        return skr;
    }

    public void setSkr(String skr) {
        this.skr = skr;
    }

    public String getRitase() {
        return ritase;
    }

    public void setRitase(String ritase) {
        this.ritase = ritase;
    }

    public String getInsentif() {
        return insentif;
    }

    public void setInsentif(String insentif) {
        this.insentif = insentif;
    }
}
