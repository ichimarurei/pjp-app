package com.indonesia.pjp.model;

/**
 * Created by doni.wahyu on 6/22/2019.
 */

public class Cuti {
    private String proyek;
    private String id;
    private String biodata;
    private String waktu;
    private String selama;
    private String status;
    private String aksi;

    public Cuti() {
    }

    public String getProyek() {
        return proyek;
    }

    public void setProyek(String proyek) {
        this.proyek = proyek;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBiodata() {
        return biodata;
    }

    public void setBiodata(String biodata) {
        this.biodata = biodata;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getSelama() {
        return selama;
    }

    public void setSelama(String selama) {
        this.selama = selama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAksi() {
        return aksi;
    }

    public void setAksi(String aksi) {
        this.aksi = aksi;
    }
}
