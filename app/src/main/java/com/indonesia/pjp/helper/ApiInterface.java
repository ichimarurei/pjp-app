package com.indonesia.pjp.helper;


import com.indonesia.pjp.response.ArsipResponse;
import com.indonesia.pjp.response.BiodataResponse;
import com.indonesia.pjp.response.CutiResponse;
import com.indonesia.pjp.response.KarirResponse;
import com.indonesia.pjp.response.KaryawanResponse;
import com.indonesia.pjp.response.KasKecilResponse;
import com.indonesia.pjp.response.KlaimResponse;
import com.indonesia.pjp.response.KpiResponse;
import com.indonesia.pjp.response.LemburResponse;
import com.indonesia.pjp.response.LiveLocationResponse;
import com.indonesia.pjp.response.LoginResponse;
import com.indonesia.pjp.response.PresensiResponse;
import com.indonesia.pjp.response.ProyekDetailResponse;
import com.indonesia.pjp.response.ProyekResponse;
import com.indonesia.pjp.response.PulangResponse;
import com.indonesia.pjp.response.RekapDetailResponse;
import com.indonesia.pjp.response.RekapResponse;
import com.indonesia.pjp.response.ShipmentResponse;
import com.indonesia.pjp.response.SimpanResponse;
import com.indonesia.pjp.response.SuratPeringatanResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by DONI on 2/13/2018.
 */

public interface ApiInterface {
    @POST("masuk")
    @FormUrlEncoded
    Call<LoginResponse> login(@Field("id-input") String username, @Field("pin-input") String password);

    @POST("detail")
    @FormUrlEncoded
    Call<BiodataResponse> biodata(@Field("param") String param, @Field("kode") String kode);

    @GET("tabel/proyek")
    Call<ProyekResponse> getProyekList();

    //    @GET("tabel/bio/s_1___p_a")
    @GET("tabel/bio/s_1___p_{proyekId}")
    Call<KaryawanResponse> getKaryawanList(@Path("proyekId") String proyekId);

    @GET("tabel/presensi/{proyekKode}___{karyawanKode}___{startDate}___{endDate}")
    Call<PresensiResponse> getPresensiList(@Path("proyekKode") String proyekKode, @Path("karyawanKode") String karyawanKode,
                                           @Path("startDate") String startDate, @Path("endDate") String endDate);

    @GET("tabel/lembur/{proyekKode}___{karyawanKode}___{startDate}___{endDate}")
    Call<LemburResponse> getLemburList(@Path("proyekKode") String proyekKode, @Path("karyawanKode") String karyawanKode,
                                       @Path("startDate") String startDate, @Path("endDate") String endDate);

    @GET("tabel/pulang/{proyekKode}___{karyawanKode}___{startDate}___{endDate}")
    Call<PulangResponse> getPulangList(@Path("proyekKode") String proyekKode, @Path("karyawanKode") String karyawanKode,
                                       @Path("startDate") String startDate, @Path("endDate") String endDate);

    @GET("tabel/presensi/all")
    Call<PresensiResponse> getPresensiListAll();

    //    @GET("tabel/cuti/{proyekKode}___all")
    @GET("tabel/cuti/{proyekKode}___{karyawanKode}")
    Call<CutiResponse> getCutiList(@Path("proyekKode") String proyekKode, @Path("karyawanKode") String karyawanKode);

    @GET("tabel/{klaimKasbon}/{proyekKode}")
    Call<KlaimResponse> getKlaimList(@Path("klaimKasbon") String klaimKasbon, @Path("proyekKode") String proyekKode);

    @GET("tabel/qty/{proyekKode}")
    Call<ShipmentResponse> getShipmentList(@Path("proyekKode") String proyekKode);


    /*@GET("tabel/klaim/{proyekKode}")
    Call<KasbonResponse> getKlaimList(@Path("proyekKode") String proyekKode);*/

    // {"status":1,"pesan":"Proses Berhasil"}
    @POST("simpan")
    @FormUrlEncoded
    Call<SimpanResponse> simpan(@FieldMap HashMap<String, String> hashFields);
     /*HashMap<String, String> map = new HashMap<>();
     map.put("question1", answer1);
     map.put("question2", answer2);
     map.put("question3", answer3);*/

    @POST("detail")
    @FormUrlEncoded
    Call<ProyekDetailResponse> proyekDetail(@Field("param") String param, @Field("kode") String kode);

    @POST("gambar")
    @FormUrlEncoded
    Call<SimpanResponse> uploadGambar(@Field("kode-text") String kode_text, @Field("gambar-text") String gambar_text
            , @Field("dir-text") String dir_text);

    @GET("tabel/kacil/lihat___{proyekKode}")
    Call<KasKecilResponse> getKasList(@Path("proyekKode") String proyekKode);

    @GET("tabel/spsl/sp___{proyekKode}")
    Call<SuratPeringatanResponse> getSuratPeringatanList(@Path("proyekKode") String proyekKode);

    @GET("tabel/rekap/{proyekKode}___{rekapType}")
    Call<RekapResponse> getRekap(@Path("proyekKode") String proyekKode,
                                 @Path("rekapType") String rekapType);

    @GET("tabel/arsip/{rekapKode}___{tahap}")
    Call<ArsipResponse> getArsip(@Path("rekapKode") String proyekKode,
                                 @Path("tahap") String tahap);

    @POST("detail")
    @FormUrlEncoded
    Call<RekapDetailResponse> rekapDetail(@Field("param") String param, @Field("kode") String kode);

    @POST("lokasi")
    @FormUrlEncoded
    Call<LiveLocationResponse> liveLocation(@Field("proyek-kode") String proyekKode, @Field("biodata-kode") String biodataKode);

    @POST("karir")
    @FormUrlEncoded
    Call<KarirResponse> karir(@Field("proyek-kode") String proyekKode, @Field("biodata-kode") String biodataKode
            , @Field("bulan-digit") String bulanDigit);

    @GET("tabel/kpi/pegawai___{proyekKode}")
    Call<KpiResponse> getKpi(@Path("proyekKode") String proyekKode);
}