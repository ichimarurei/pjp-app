package com.indonesia.pjp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.pjp.R;
import com.indonesia.pjp.adapter.RekapAdapter;
import com.indonesia.pjp.fragment.ProyekFilterBottomSheetDialog;
import com.indonesia.pjp.helper.ClickListener;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.model.Rekap;
import com.indonesia.pjp.response.RekapResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RekapActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener
        , ProyekFilterBottomSheetDialog.BottomSheetFilterListener {

    String proyekId = "", proyekText = "", rekapType = "";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.filterConstraintLayout)
    public void filterConstraintLayoutCLick(ConstraintLayout constraintLayout) {
        openFilterBottomSheet();
    }

    @BindView(R.id.filterConstraintLayout)
    ConstraintLayout filterConstraintLayout;

    SearchView searchView;
    List<Rekap> rekapList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;

        if (getIntent().hasExtra("rekapType")) {
            rekapType = getIntent().getStringExtra("rekapType");
        }

        getSupportActionBar().setTitle("Data " + rekapType);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_PEGAWAI)
                ) {
            filterConstraintLayout.setVisibility(View.GONE);
        } else {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && filterConstraintLayout.getVisibility() == View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animHide);
                    } else if (dy < 0 && filterConstraintLayout.getVisibility() != View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animShow);
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRekapData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getRekapData();
    }

    void getRekapData() {
        rekapList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        recyclerView.setAdapter(null);

        String proyekKode = "0";

        if (proyekId.length() > 0) {
            proyekKode = proyekId;
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            proyekKode = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
        }


        String type = rekapType;
        if (rekapType.equalsIgnoreCase(Constants.MENU_REKAP_LEMBUR)) {
            type = "lembur";
        } else {
            type = "presensi";
        }

        Call<RekapResponse> service = api.getRekap(proyekKode, type);

        service.enqueue(new Callback<RekapResponse>() {
            @Override
            public void onResponse(Call<RekapResponse> call, Response<RekapResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                RekapResponse rekapResponse = response.body();
                rekapList = rekapResponse.getRekapList();
                setAdapterDataRecycleView("");
            }

            @Override
            public void onFailure(Call<RekapResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                call.cancel();
                toast(Constants.SERVER_RESPONSE_ERROR);
            }
        });
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Rekap> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Rekap rekap : rekapList) {
                if (rekap.getStatus().toLowerCase().contains(keyword)) {
                    filterList.add(rekap);
                }
            }
        } else {
            filterList.addAll(rekapList);
        }

        recyclerView.setAdapter(new RekapAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String type;
                if (rekapType.equalsIgnoreCase(Constants.MENU_REKAP_LEMBUR)) {
                    type = "lembur";
                } else {
                    type = "presensi";
                }

                startActivity(new Intent(getBaseContext(),
                        ArsipActivity.class)
                        .putExtra("rekapKode", filterList.get(position).getKode())
                        .putExtra("tahap", filterList.get(position).getTahap())
                        .putExtra("rekapType", type)
                );
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    void openFilterBottomSheet() {
        Bundle bundle = new Bundle();
        bundle.putString("proyekId", proyekId);
        bundle.putString("proyekText", proyekText);

        ProyekFilterBottomSheetDialog bottomSheet = new ProyekFilterBottomSheetDialog();
        bottomSheet.setArguments(bundle);
        bottomSheet.setListener(this);
        bottomSheet.show(getSupportFragmentManager(), "proyekFilterBottomSheet");
    }

    @Override
    public void onButtonFilterClicked(String proyekId, String proyekText) {
        this.proyekId = proyekId;
        this.proyekText = proyekText;
        getRekapData();
    }

    @Override
    public void onButtonrResetClicked() {
        this.proyekId = "";
        this.proyekText = "";
        getRekapData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }
}
