package com.indonesia.pjp.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.pjp.R;
import com.indonesia.pjp.adapter.CutiAdapter;
import com.indonesia.pjp.fragment.ProyekFilterBottomSheetDialog;
import com.indonesia.pjp.helper.ClickListener;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.model.Cuti;
import com.indonesia.pjp.response.CutiResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CutiActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener
        , ProyekFilterBottomSheetDialog.BottomSheetFilterListener {

    String proyekId = "", proyekText = "";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.filterConstraintLayout)
    public void filterConstraintLayoutCLick(ConstraintLayout constraintLayout) {
        openFilterBottomSheet();
    }

    @BindView(R.id.filterConstraintLayout)
    ConstraintLayout filterConstraintLayout;

    SearchView searchView;
    List<Cuti> cutiList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuti);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Cuti");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_PEGAWAI)
                ) {
            filterConstraintLayout.setVisibility(View.GONE);
        } else {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && filterConstraintLayout.getVisibility() == View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animHide);
                    } else if (dy < 0 && filterConstraintLayout.getVisibility() != View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animShow);
                    }
                }
            });
        }

        getCutiData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getCutiData();
    }

    void getCutiData() {
        cutiList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        recyclerView.setAdapter(null);

        String proyekKode = "all";
        String karyawanKode = "all";

        if (proyekId.length() > 0) {
            proyekKode = proyekId;
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            proyekKode = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_PEGAWAI)) {
            karyawanKode = sessionManager.retrieve(SessionManager.BIO);
        }

        Call<CutiResponse> service = api.getCutiList(proyekKode, karyawanKode);

        service.enqueue(new Callback<CutiResponse>() {
            @Override
            public void onResponse(Call<CutiResponse> call, Response<CutiResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                CutiResponse cutiResponse = response.body();
                cutiList = cutiResponse.getCutiList();
                setAdapterDataRecycleView("");
            }

            @Override
            public void onFailure(Call<CutiResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                call.cancel();
                toast(Constants.SERVER_RESPONSE_ERROR);
            }
        });
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Cuti> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Cuti cuti : cutiList) {
                if (cuti.getBiodata().toLowerCase().contains(keyword)) {
                    filterList.add(cuti);
                }
            }
        } else {
            filterList.addAll(cutiList);
        }

        recyclerView.setAdapter(new CutiAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                        /*startActivity(new Intent(getApplicationContext(), CutiDetailActivity.class)
                                .putExtra("newsId", newsList.get(position).getCutiId()));*/
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    void openFilterBottomSheet() {
        Bundle bundle = new Bundle();
        bundle.putString("proyekId", proyekId);
        bundle.putString("proyekText", proyekText);

        ProyekFilterBottomSheetDialog bottomSheet = new ProyekFilterBottomSheetDialog();
        bottomSheet.setArguments(bundle);
        bottomSheet.setListener(this);
        bottomSheet.show(getSupportFragmentManager(), "proyekFilterBottomSheet");
    }

    @Override
    public void onButtonFilterClicked(String proyekId, String proyekText) {
        this.proyekId = proyekId;
        this.proyekText = proyekText;
        getCutiData();
    }

    @Override
    public void onButtonrResetClicked() {
        this.proyekId = "";
        this.proyekText = "";
        getCutiData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }
}
