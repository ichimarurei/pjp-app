package com.indonesia.pjp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.pjp.R;
import com.indonesia.pjp.adapter.PresensiAdapter;
import com.indonesia.pjp.fragment.PresensiFilterBottomSheetDialog;
import com.indonesia.pjp.helper.ClickListener;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.model.Presensi;
import com.indonesia.pjp.response.PresensiResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresensiActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener
        , PresensiFilterBottomSheetDialog.BottomSheetFilterListener {

    String requestType = "", proyekId = "", proyekText = "", startDate = "", endDate = "";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.filterConstraintLayout)
    public void filterConstraintLayoutCLick(ConstraintLayout constraintLayout) {
        openFilterBottomSheet();
    }

    @BindView(R.id.filterConstraintLayout)
    ConstraintLayout filterConstraintLayout;

    SearchView searchView;
    List<Presensi> presensiList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presensi);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Absensi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_PEGAWAI)
                ) {
            filterConstraintLayout.setVisibility(View.GONE);
        } else {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && filterConstraintLayout.getVisibility() == View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animHide);
                    } else if (dy < 0 && filterConstraintLayout.getVisibility() != View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animShow);
                    }
                }
            });
        }

        if (getIntent().hasExtra(Constants.REQUEST_TYPE)) {
            requestType = getIntent().getStringExtra(Constants.REQUEST_TYPE);
        }

        if (getIntent().hasExtra("proyekId")) {
            proyekId = getIntent().getStringExtra("proyekId");
            proyekText = getIntent().getStringExtra("proyekText");
        }

        getPresensiData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getPresensiData();
    }

    void getPresensiData() {
        presensiList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        recyclerView.setAdapter(null);

        String proyekKode = "all";
        String karyawanKode = "all";
        String startDateFilter = "x";
        String endDateFilter = "x";

        if (proyekId.length() > 0) {
            proyekKode = proyekId;
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            proyekKode = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_PEGAWAI)) {
            karyawanKode = sessionManager.retrieve(SessionManager.BIO);
        }

        if (startDate.length() > 0) {
            startDateFilter = startDate;
        }

        if (endDate.length() > 0) {
            endDateFilter = endDate;
        }

        Call<PresensiResponse> service = api.getPresensiList(proyekKode, karyawanKode, startDateFilter, endDateFilter);

        service.enqueue(new Callback<PresensiResponse>() {
            @Override
            public void onResponse(Call<PresensiResponse> call, Response<PresensiResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                PresensiResponse presensiResponse = response.body();
                presensiList = presensiResponse.getPresensiList();
                setAdapterDataRecycleView("");
            }

            @Override
            public void onFailure(Call<PresensiResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                call.cancel();
                toast(Constants.SERVER_RESPONSE_ERROR);
            }
        });
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Presensi> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Presensi presensi : presensiList) {
                if (presensi.getBiodata().toLowerCase().contains(keyword)) {
                    filterList.add(presensi);
                }
            }
        } else {
            filterList.addAll(presensiList);
        }

        recyclerView.setAdapter(new PresensiAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (requestType.equalsIgnoreCase(Constants.REQUEST_INTENT_RESULT)) {
                    Intent intent = new Intent();
                    intent.putExtra("id", filterList.get(position).getKode());
                    intent.putExtra("text", filterList.get(position).getBiodata());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }


    void openFilterBottomSheet() {
        Bundle bundle = new Bundle();
        bundle.putString("proyekId", proyekId);
        bundle.putString("proyekText", proyekText);
        bundle.putString("startDate", startDate);
        bundle.putString("endDate", endDate);

        PresensiFilterBottomSheetDialog bottomSheet = new PresensiFilterBottomSheetDialog();
        bottomSheet.setArguments(bundle);
        bottomSheet.setListener(this);
        bottomSheet.show(getSupportFragmentManager(), "presensiFilterBottomSheet");
    }

    @Override
    public void onButtonFilterClicked(String proyekId, String proyekText, String startDate, String endDate) {
        this.proyekId = proyekId;
        this.proyekText = proyekText;
        this.startDate = startDate;
        this.endDate = endDate;
        getPresensiData();
    }

    @Override
    public void onButtonrResetClicked() {
        this.proyekId = "";
        this.proyekText = "";
        this.startDate = "";
        this.endDate = "";
        getPresensiData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }

}
