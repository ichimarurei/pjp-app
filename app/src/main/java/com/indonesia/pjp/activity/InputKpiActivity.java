package com.indonesia.pjp.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.indonesia.pjp.R;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.helper.Utility;
import com.indonesia.pjp.response.SimpanResponse;

import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputKpiActivity extends BaseActivity {

    String[][] soalSeragamList = new String[][]{
            {"p1a:3", "Topi"},
            {"p1b:3", "Baju Disetrika"},
            {"p1c:3", "Papan Nama"},
            {"p1d:4", "ID Card"},
            {"p1e:3", "Kewenangan"},
            {"p1f:3", "Pin / Badge Lotus"},
            {"p1g:3", "Kopel Dibraso / Ikat Pinggang"},
            {"p1h:3", "Sepatu Disemir"},
            {"p1i:3", "Kaos Dalam Hitam / Putih"}
    };

    String[][] soalPenampilanList = new String[][]{
            {"p2a:3", "Rambut (1-2)"},
            {"p2b:3", "Mulut Wangi"},
            {"p2c:3", "Kumis Rapi"},
            {"p2d:3", "Jenggot Tidak Ada"},
            {"p2e:3", "Kuku Tidak Panjang"},
            {"p2f:3", "Perlengkapan & Atribut Bersih"},
            {"p2g:3", "Seragam Wangi & Bersih"},
            {"p2h:3", "Aksesoris Tidak Berlebihan, Sesuai Peraturan"}
    };


    String[][] soalSikapList = new String[][]{
            {"p3a:3", "Senyum, Tidak Cengengesan"},
            {"p3b:3", "Sapa, Disesuaikan Dengan Situasi"},
            {"p3c:2", "Salam"},
            {"p3d:3", "Tegas"},
            {"p3e:4", "Patuh & Disiplin Dalam Peraturan"},
            {"p3f:4", "Patuh Terhadap Pimpinan"},
            {"p3g:3", "Banyak Bicara Yang Tidak Perlu"},
            {"p3h:3", "Hormat Sesuai Kondisi"}
    };

    String[][] soalKerajinanList = new String[][]{
            {"p4a:5", "Kelengkapan Absensi"},
            {"p4b:4", "Ketepatan Waktu Kerja"},
            {"p4c:3", "Pelaksanaan Kerja Sesuai Prosedur"},
            {"p4d:4", "Tidak Meninggalkan Plottingan Tanpa Koordinasi"},
            {"p4e:4", "Tidak Pernah Terima Surat Peringatan (SP)\n"},
            {"p4f:3", "Melaksanakan Pemantauan Area (Patroli)"},
    };


    String kodeProyek, kodeKaryawan, diketahuiOleh, disahkanOleh;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @BindView(R.id.diketahuiOlehTextView)
    TextView diketahuiOlehTextView;

    @BindView(R.id.disahkanOlehTextView)
    TextView disahkanOlehTextView;

    @BindView(R.id.tanggalTextView)
    TextView tanggalTextView;

    @BindView(R.id.seragamRelativeLayout)
    RelativeLayout seragamRelativeLayout;

    @BindView(R.id.penampilanRelativeLayout)
    RelativeLayout penampilanRelativeLayout;

    @BindView(R.id.sikapRelativeLayout)
    RelativeLayout sikapRelativeLayout;

    @BindView(R.id.kerajinanRelativeLayout)
    RelativeLayout kerajinanRelativeLayout;

    @OnClick(R.id.tanggalTextView)
    public void tanggalTextViewClick(TextView textView) {
        openCalendar();
    }

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
        }
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    @OnClick(R.id.diketahuiOlehTextView)
    public void diketahuiOlehTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_DIKETAHUI);
    }

    @OnClick(R.id.disahkanOlehTextView)
    public void disahkanOlehTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_DISAHKAN);
    }

    boolean isValidInputSoal() {
        TableLayout seragamTableLayout = (TableLayout) seragamRelativeLayout.getChildAt(0);
        for (int i = 0; i < soalSeragamList.length; i++) {
            TableRow row1 = (TableRow) seragamTableLayout.getChildAt(i);
            EditText et = (EditText) row1.getChildAt(1);
            String value = et.getText().toString();

            if (value.isEmpty()) {
                et.requestFocus();
                return false;
            }
        }

        TableLayout penampilanTableLayout = (TableLayout) penampilanRelativeLayout.getChildAt(0);
        for (int i = 0; i < soalPenampilanList.length; i++) {
            TableRow row1 = (TableRow) penampilanTableLayout.getChildAt(i);
            EditText et = (EditText) row1.getChildAt(1);
            String value = et.getText().toString();

            if (value.isEmpty()) {
                et.requestFocus();
                return false;
            }
        }

        TableLayout sikapTableLayout = (TableLayout) sikapRelativeLayout.getChildAt(0);
        for (int i = 0; i < soalSikapList.length; i++) {
            TableRow row1 = (TableRow) sikapTableLayout.getChildAt(i);
            EditText et = (EditText) row1.getChildAt(1);
            String value = et.getText().toString();

            if (value.isEmpty()) {
                et.requestFocus();
                return false;
            }
        }

        TableLayout kerajinanTableLayout = (TableLayout) kerajinanRelativeLayout.getChildAt(0);
        for (int i = 0; i < soalKerajinanList.length; i++) {
            TableRow row1 = (TableRow) kerajinanTableLayout.getChildAt(i);
            EditText et = (EditText) row1.getChildAt(1);
            String value = et.getText().toString();

            if (value.isEmpty()) {
                et.requestFocus();
                return false;
            }
        }
        return true;
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (isValidInputSoal()) {
            if (Utility.isEmpty(proyekTextView)) {
                toast("Proyek tidak boleh kosong");
            } else if (Utility.isEmpty(karyawanTextView)) {
                toast("Karyawan tidak boleh kosong");
            } else if (Utility.isEmpty(tanggalTextView)) {
                toast("Tanggal Kpi tidak boleh kosong");
            } else {
                Integer pointInput = 0;
                StringBuilder resultJawaban = new StringBuilder();

                TableLayout seragamTableLayout = (TableLayout) seragamRelativeLayout.getChildAt(0);
                for (int i = 0; i < soalSeragamList.length; i++) {
                    String[] soalSeragam = soalSeragamList[i];
                    TableRow row1 = (TableRow) seragamTableLayout.getChildAt(i);
                    EditText et = (EditText) row1.getChildAt(1);
                    String value = et.getText().toString();

                    pointInput += Integer.parseInt(value);
                    resultJawaban.append(soalSeragam[0]).append("=").append(value).append(",");
                }

                TableLayout penampilanTableLayout = (TableLayout) penampilanRelativeLayout.getChildAt(0);
                for (int i = 0; i < soalPenampilanList.length; i++) {
                    String[] soalPenampilan = soalPenampilanList[i];
                    TableRow row1 = (TableRow) penampilanTableLayout.getChildAt(i);
                    EditText et = (EditText) row1.getChildAt(1);
                    String value = et.getText().toString();

                    pointInput += Integer.parseInt(value);
                    resultJawaban.append(soalPenampilan[0]).append("=").append(value).append(",");
                }

                TableLayout sikapTableLayout = (TableLayout) sikapRelativeLayout.getChildAt(0);
                for (int i = 0; i < soalSikapList.length; i++) {
                    String[] soalSikap = soalSikapList[i];
                    TableRow row1 = (TableRow) sikapTableLayout.getChildAt(i);
                    EditText et = (EditText) row1.getChildAt(1);
                    String value = et.getText().toString();

                    pointInput += Integer.parseInt(value);
                    resultJawaban.append(soalSikap[0]).append("=").append(value).append(",");
                }

                TableLayout kerajinanTableLayout = (TableLayout) kerajinanRelativeLayout.getChildAt(0);
                for (int i = 0; i < soalKerajinanList.length; i++) {
                    String[] soalKerajinan = soalKerajinanList[i];
                    TableRow row1 = (TableRow) kerajinanTableLayout.getChildAt(i);
                    EditText et = (EditText) row1.getChildAt(1);
                    String value = et.getText().toString();

                    pointInput += Integer.parseInt(value);
                    resultJawaban.append(soalKerajinan[0]).append("=").append(value);

                    if (i < soalKerajinanList.length - 1) {
                        resultJawaban.append(",");
                    }
                }

                presensiProcess(pointInput, resultJawaban);
            }

        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_kpi);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Kpi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }

        TableLayout tableSeragam = new TableLayout(getApplicationContext());
        tableSeragam.setStretchAllColumns(true);
        for (int i = 0; i < soalSeragamList.length; i++) {
            String[] soalSeragam = soalSeragamList[i];

            TableRow[] tableRow = new TableRow[soalSeragamList.length];
            tableRow[i] = new TableRow(getApplicationContext());
            tableRow[i].setGravity(Gravity.CENTER);

            TextView label = new TextView(getApplicationContext());
            label.setGravity(Gravity.LEFT);
            label.setText(soalSeragam[1]);

            EditText input = new EditText(getApplicationContext());
            input.setGravity(Gravity.CENTER);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);

            TextView labelMax = new TextView(getApplicationContext());
            labelMax.setGravity(Gravity.LEFT);
            labelMax.setText(" /" + soalSeragam[0].split(":")[1]);

            tableRow[i].addView(label);
            tableRow[i].addView(input);
            tableRow[i].addView(labelMax);

            tableSeragam.addView(tableRow[i]);
        }
        seragamRelativeLayout.addView(tableSeragam);

        TableLayout tablePenampilan = new TableLayout(getApplicationContext());
        tablePenampilan.setStretchAllColumns(true);
        for (int i = 0; i < soalPenampilanList.length; i++) {
            String[] soalPenampilan = soalPenampilanList[i];

            TableRow[] tableRow = new TableRow[soalPenampilanList.length];
            tableRow[i] = new TableRow(getApplicationContext());
            tableRow[i].setGravity(Gravity.CENTER);

            TextView label = new TextView(getApplicationContext());
            label.setGravity(Gravity.LEFT);
            label.setText(soalPenampilan[1]);

            EditText input = new EditText(getApplicationContext());
            input.setGravity(Gravity.CENTER);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);

            TextView labelMax = new TextView(getApplicationContext());
            labelMax.setGravity(Gravity.LEFT);
            labelMax.setText(" /" + soalPenampilan[0].split(":")[1]);

            tableRow[i].addView(label);
            tableRow[i].addView(input);
            tableRow[i].addView(labelMax);

            tablePenampilan.addView(tableRow[i]);
        }
        penampilanRelativeLayout.addView(tablePenampilan);


        TableLayout tableSikap = new TableLayout(getApplicationContext());
        tableSikap.setStretchAllColumns(true);
        for (int i = 0; i < soalSikapList.length; i++) {
            String[] soalSikap = soalSikapList[i];

            TableRow[] tableRow = new TableRow[soalSikapList.length];
            tableRow[i] = new TableRow(getApplicationContext());
            tableRow[i].setGravity(Gravity.CENTER);

            TextView label = new TextView(getApplicationContext());
            label.setGravity(Gravity.LEFT);
            label.setText(soalSikap[1]);

            EditText input = new EditText(getApplicationContext());
            input.setGravity(Gravity.CENTER);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);

            TextView labelMax = new TextView(getApplicationContext());
            labelMax.setGravity(Gravity.LEFT);
            labelMax.setText(" /" + soalSikap[0].split(":")[1]);

            tableRow[i].addView(label);
            tableRow[i].addView(input);
            tableRow[i].addView(labelMax);

            tableSikap.addView(tableRow[i]);
        }
        sikapRelativeLayout.addView(tableSikap);


        TableLayout tableKerajinan = new TableLayout(getApplicationContext());
        tableKerajinan.setStretchAllColumns(true);
        for (int i = 0; i < soalKerajinanList.length; i++) {
            String[] soalKerajinan = soalKerajinanList[i];

            TableRow[] tableRow = new TableRow[soalKerajinanList.length];
            tableRow[i] = new TableRow(getApplicationContext());
            tableRow[i].setGravity(Gravity.CENTER);

            TextView label = new TextView(getApplicationContext());
            label.setGravity(Gravity.LEFT);
            label.setText(soalKerajinan[1]);

            EditText input = new EditText(getApplicationContext());
            input.setGravity(Gravity.CENTER);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);

            TextView labelMax = new TextView(getApplicationContext());
            labelMax.setGravity(Gravity.LEFT);
            labelMax.setText(" /" + soalKerajinan[0].split(":")[1]);

            tableRow[i].addView(label);
            tableRow[i].addView(input);
            tableRow[i].addView(labelMax);

            tableKerajinan.addView(tableRow[i]);
        }
        kerajinanRelativeLayout.addView(tableKerajinan);
    }

    private void presensiProcess(Integer pointInput, StringBuilder resultJawaban) {
        showProgressDialog();
 
        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "kpi");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", Utility.generateUUID());
        map.put("terpakai-input", "1");
        map.put("level-input", "pegawai");
        map.put("pembuat-input", sessionManager.retrieve(SessionManager.BIO));
        map.put("proyek-input", kodeProyek);
        map.put("pegawai-input", kodeKaryawan);
        map.put("pengawas-input", diketahuiOleh);
        map.put("penerima-input", disahkanOleh);
        map.put("tanggal-input", Utility.getText(tanggalTextView));
        map.put("poin-input", String.valueOf(pointInput));
        map.put("kpi-input", String.valueOf(resultJawaban));

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Input KPI Berhasil");
                        finish();
                    } else {
                        toast("Input KPI Gagal");
                    }
                } else {
                    toast("Input KPI Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Input KPI Gagal");
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_DIKETAHUI) {
                diketahuiOleh = data.getStringExtra("id");
                diketahuiOlehTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_DISAHKAN) {
                disahkanOleh = data.getStringExtra("id");
                disahkanOlehTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    void openCalendar() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(InputKpiActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String sMonth = String.valueOf(monthOfYear);
                        if (sMonth.length() == 1) {
                            sMonth = "0" + (Integer.parseInt(sMonth) + 1);
                            sMonth = sMonth.substring(sMonth.length() - 2);
                        }

                        String sDay = String.valueOf(dayOfMonth);
                        if (sDay.length() == 1) {
                            sDay = "0" + sDay;
                        }

                        tanggalTextView.setText(sDay + "/" + sMonth + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
