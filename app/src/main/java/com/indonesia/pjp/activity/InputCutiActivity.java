package com.indonesia.pjp.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.indonesia.pjp.R;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.helper.Utility;
import com.indonesia.pjp.response.SimpanResponse;

import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputCutiActivity extends BaseActivity {

    String kodeProyek, kodeKaryawan;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @BindView(R.id.tanggalCutiTextView)
    TextView tanggalCutiTextView;

    @BindView(R.id.keteranganEditText)
    EditText keteranganEditText;

    @BindView(R.id.lamaCutiEditText)
    EditText lamaCutiEditText;

    @OnClick(R.id.tanggalCutiTextView)
    public void tanggalCutiTextViewClick(TextView textView) {
        openCalendar();
    }

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
        }
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(karyawanTextView)) {
            toast("Karyawan tidak boleh kosong");
        } else if (Utility.isEmpty(tanggalCutiTextView)) {
            toast("Tanggal Cuti tidak boleh kosong");
        } else if (Utility.isEmpty(lamaCutiEditText)) {
            toast("Lamat Cuti tidak boleh kosong");
        } else {
            presensiProcess();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_cuti);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Cuti");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }
    }

    private void presensiProcess() {
        showProgressDialog();

        /*model-input:cuti
            action-input:1
            key-input:0
            kode-input:3c1f3699c741bf5cea7cc9f373jms215
            terpakai-input:1
            proyek-input:0d4dd841d8a81110adb3d05dbd334f00
            atasan-input:-
            biodata-input:60fcafc488fa932750e8f08fba9fa87d
            waktu-input:21/06/2019
            selama-input:2
            keterangan-input:Iseng
            status-input:ajuan*/

        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "cuti");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", Utility.generateUUID());
        map.put("terpakai-input", "1");
        map.put("proyek-input", kodeProyek);
        map.put("atasan-input", "-");
        map.put("biodata-input", kodeKaryawan);
        map.put("waktu-input", Utility.getText(tanggalCutiTextView));
        map.put("selama-input", Utility.getText(lamaCutiEditText));
        map.put("keterangan-input", Utility.getText(keteranganEditText));
        map.put("status-input", "ajuan");

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Cuti Berhasil");
                        finish();
                    } else {
                        toast("Cuti Gagal");
                    }
                } else {
                    toast("Cuti Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Cuti Gagal");
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    void openCalendar() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(InputCutiActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String sMonth = String.valueOf(monthOfYear);
                        if (sMonth.length() == 1) {
                            sMonth = "0" + (Integer.parseInt(sMonth) + 1);
                            sMonth = sMonth.substring(sMonth.length() - 2);
                        }

                        String sDay = String.valueOf(dayOfMonth);
                        if (sDay.length() == 1) {
                            sDay = "0" + sDay;
                        }

                        tanggalCutiTextView.setText(sDay + "/" + sMonth + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
