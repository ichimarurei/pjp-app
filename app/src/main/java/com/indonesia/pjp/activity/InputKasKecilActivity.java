package com.indonesia.pjp.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.indonesia.pjp.R;
import com.indonesia.pjp.fragment.ChooseFleFragment;
import com.indonesia.pjp.helper.Constants;
import com.indonesia.pjp.helper.SessionManager;
import com.indonesia.pjp.helper.Utility;
import com.indonesia.pjp.response.SimpanResponse;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputKasKecilActivity extends BaseActivity implements ChooseFleFragment.BottomSheetFileChooserListener {
    @BindView(R.id.proyekTextView)
    TextView proyekTextView;
    @BindView(R.id.tanggalKlaimTextView)
    TextView tanggalKlaimTextView;
    @BindView(R.id.lampiranTextView)
    TextView lampiranTextView;
    @BindView(R.id.nominalEditText)
    EditText nominalEditText;
    @BindView(R.id.perihalEditText)
    EditText perihalEditText;

    private String kodeProyek;
    private File lampiranFile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_kas_kecil);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Pengeluaran Kas");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();

        return true;
    }

    @Override
    public void onFileSelected(File imageFile) {
        lampiranTextView.setText(imageFile.getName());
        lampiranFile = imageFile;
    }

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
        }
    }

    @OnClick(R.id.lampiranTextView)
    public void lampiranTextViewClick(TextView textView) {
        ChooseFleFragment chooseFleFragment = new ChooseFleFragment();
        chooseFleFragment.show(getSupportFragmentManager(), "chooseFleFragment");
        chooseFleFragment.setListener(this);
    }

    @OnClick(R.id.tanggalKlaimTextView)
    public void tanggalKlaimTextViewClick(TextView textView) {
        openCalendar();
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(tanggalKlaimTextView)) {
            toast("Tanggal tidak boleh kosong");
        } else if (Utility.isEmpty(nominalEditText)) {
            toast("Nominal tidak boleh kosong");
        } else if (Utility.isEmpty(perihalEditText)) {
            toast("Perihal/Keterangan Pengeluaran tidak boleh kosong");
        } else if (lampiranFile == null) {
            toast("File Lampiran harus dipilih");
        } else {
            doSave();
        }
    }

    private void doSave() {
        showProgressDialog();
        final String UUID = Utility.generateUUID();
        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "kacil");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("terpakai-input", "1");
        map.put("jenis-input", "kredit");
        map.put("kode-input", UUID);
        map.put("biodata-input", sessionManager.retrieve(SessionManager.BIO));
        map.put("waktu-input", Utility.now());
        map.put("proyek-input", kodeProyek);
        map.put("tanggal-input", Utility.getText(tanggalKlaimTextView));
        map.put("nominal-input", Utility.getText(nominalEditText));
        map.put("perihal-input", Utility.getText(perihalEditText));
        Call<SimpanResponse> service = api.simpan(map);
        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                SimpanResponse simpanResponse = response.body();
                boolean isOK = false;

                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        uploadLampiran(UUID);
                        isOK = true;
                    }
                }

                if (!isOK) {
                    dismissProgressDialog();
                    toast("Proses Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                toast("Proses Gagal");
                call.cancel();
            }
        });
    }

    private void uploadLampiran(String UUID) {
        File compressFile = Utility.compressFile(getApplicationContext(), lampiranFile);
        String gambar = Utility.imageToBase64(compressFile);
        Call<SimpanResponse> service = api.uploadGambar(UUID, gambar, "keuangan");
        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                boolean isOK = false;
                SimpanResponse simpanResponse = response.body();

                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Proses Berhasil");
                        isOK = true;
                        finish();
                    }
                }

                if (!isOK) {
                    toast("Upload Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                toast("Upload Gagal");
                call.cancel();
            }
        });
    }

    private void openCalendar() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(InputKasKecilActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String sMonth = String.valueOf(monthOfYear);
                        if (sMonth.length() == 1) {
                            sMonth = "0" + (Integer.parseInt(sMonth) + 1);
                            sMonth = sMonth.substring(sMonth.length() - 2);
                        }

                        String sDay = String.valueOf(dayOfMonth);
                        if (sDay.length() == 1) {
                            sDay = "0" + sDay;
                        }

                        tanggalKlaimTextView.setText(sDay + "/" + sMonth + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
