package com.indonesia.pjp.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.pjp.model.KasKecil;

import java.util.List;

public class KasKecilResponse {
    @SerializedName("data")
    @Expose
    private List<KasKecil> kasList;

    public KasKecilResponse() {
    }

    public List<KasKecil> getKasList() {
        return kasList;
    }

    public void setKasList(List<KasKecil> kasList) {
        this.kasList = kasList;
    }
}
