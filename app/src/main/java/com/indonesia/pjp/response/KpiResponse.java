package com.indonesia.pjp.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.pjp.model.Kpi;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class KpiResponse {
    @SerializedName("data")
    @Expose
    private List<Kpi> kpiList;

    public KpiResponse() {
    }

    public List<Kpi> getKpiList() {
        return kpiList;
    }

    public void setKpiList(List<Kpi> kpiList) {
        this.kpiList = kpiList;
    }
}
