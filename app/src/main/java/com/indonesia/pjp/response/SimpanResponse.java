package com.indonesia.pjp.response;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class SimpanResponse extends BaseResponse {
    private String pesan;

    public SimpanResponse() {
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
