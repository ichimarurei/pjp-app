package com.indonesia.pjp.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.pjp.model.SuratPeringatan;

import java.util.List;

public class SuratPeringatanResponse {
    @SerializedName("data")
    @Expose
    private List<SuratPeringatan> suratPeringatanList;

    public SuratPeringatanResponse() {
    }

    public List<SuratPeringatan> getSuratPeringatanList() {
        return suratPeringatanList;
    }

    public void setSuratPeringatanList(List<SuratPeringatan> suratPeringatanList) {
        this.suratPeringatanList = suratPeringatanList;
    }
}
